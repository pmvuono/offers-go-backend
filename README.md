TITULO
======

Sistema de Gestón de ofertas cercanas a la localización del usuario

DESCRIPCION
===========

Repositorio publico del Trabajo Final de Graduacion (Backend)

------------------------------------------------------------------

REQUISITOS
==========

- Un entorno de desarrollo web: Apache, IIS, Nginx PHP 5.3 o superior.
- Base de datos: MySQL, Sqlite, Postgresql o sqlserver.
- Librerías php: Mcrypt.
- Para probar es recomendado usa Postman o alguno similar.

------------------------------------------------------------------

USO
===

Para tener un vistazo de como se usa ver el documento instructivo-backend.pdf

------------------------------------------------------------------

| ALUMNO            | LEGAJO    |
| ----------------- | --------- |
| Pablo Martn Vuono | VINF07144 |