<?php namespace App\OffersGo;

use Exception;

abstract class BaseService
{
    /* Private Properties
    -------------------------------*/

    /**
     * Datos del Servicio
     */
    private $_data = [];

    /**
     * Nombre del Servicio
     */
    private $_name;

    /* Public Functions
    -------------------------------*/

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(string $serviceName)
    {
        $this->_name = $serviceName;
    }

    /**
     * Función que devuelve los datos del servicio
     *
     * @param array
     * @return void
     */
    final public function get(array &$data)
    {
        $data = $this->_data;
    }

    /**
     * Función de procesamiento del servicio
     *
     * @param array $data
     * @return BaseService
     * @throws Exception
     */
    final public function process(array $data) : BaseService
    {
        $process = 'process' . $data['method'];

        if (!method_exists($this->_name, $process))
            throw new Exception(trans('service.func_not_exist', ['function' => $process]));

        $this->$process($data['data']);

        return $this;
    }

    /* Protected Functions
    -------------------------------*/

    /**
     * Función que carga los datos del servicio
     *
     * @param array
     * @return void
     */
    final protected function _set(array $data)
    {
        $this->_data = $data;
    }
}
