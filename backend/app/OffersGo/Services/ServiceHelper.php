<?php namespace App\OffersGo\Helpers;

use App\OffersGo\BaseService;
use Exception;

final class ServiceHelper
{
    /* Private Properties
    -------------------------------*/

    /**
     * Servicios
     */
    private static $_data = array();

    /* Public Functions
    -------------------------------*/

    /**
     * Función que devuelve el servicio llamado
     *
     * @param string
     * @return BaseService
     * @throws Exception
     */
    public static function make(string $name) : BaseService
    {
        if (!in_array($name, self::$_data)) self::_createService($name);

        return self::$_data[$name];
    }

    /* Private Functions
    -------------------------------*/

    /**
     * Función que devuelve los datos en Response
     *
     * @param string
     * @param array
     * @return void
     * @throws Exception
     */
    private static function _createService(string $name)
    {
        $class = '\\App\\OffersGo\\Services\\' . $name . 'Service';

        if (!class_exists($class))
            throw new Exception(trans('service.service_not_exist', ['service' => $name . 'Service']));

        self::$_data[$name] = new $class($class);
    }
}
