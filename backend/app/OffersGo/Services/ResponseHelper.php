<?php namespace App\OffersGo\Helpers;

use Illuminate\Support\Facades\Config;
use Symfony\Component\HttpFoundation\Response;

final class ResponseHelper
{
    /* Public Functions
    -------------------------------*/

    /**
     * Función que devuelve los datos en Response
     *
     * @param string $message
     * @param int $code
     * @param string|null $extras
     * @return Response
     */
    public static function errorRequest(string $message,
                                        int $code = Response::HTTP_BAD_REQUEST,
                                        string $extras = null) : Response
    {
        if ($code == 0) 
        {
            $code = Response::HTTP_INTERNAL_SERVER_ERROR;
        }

        if ($extras == null) 
        {
            return response()->json([
                        'status' => Config::get('offers-go.status.error'),
                        'message' => $message
                            ], $code);
        }

        return response()->json([
            'status'  => Config::get('offers-go.status.error'),
            'message' => $message,
            'extras'  => $extras
        ], $code);
    }

    /**
     * Función que devuelve los datos en Response
     *
     * @param string
     * @return Response
     */
    public static function successOk(string $message) : Response
    {
        return response()->json([
            'status'  => Config::get('offers-go.status.success'),
            'message' => $message
        ], Response::HTTP_OK);
    }

    /**
     * Función que devuelve los datos en Response
     *
     * @param string
     * @param array|null $data
     * @return Response
     */
    public static function successOkWithData(string $message,
                                             array $data = null) : Response
    {
        return response()->json([
            'status'  => Config::get('offers-go.status.success'),
            'message' => $message,
            'data'    => $data
        ], Response::HTTP_OK);
    }

    /**
     * Función que devuelve los datos en Response
     *
     * @param string
     * @param array|null $data
     * @param array|null $extras
     * @return Response
     */
    public static function successOkWithDataAndExtras(string $message,
                                                      array $data = null,
                                                      array $extras = null) : Response
    {
        return response()->json([
            'status'  => Config::get('offers-go.status.success'),
            'message' => $message,
            'data'    => $data,
            'extras'  => $extras
        ], Response::HTTP_OK);
    }

    /**
     * Función que devuelve los datos en Response
     *
     * @param string
     * @param null $data
     * @return Response
     */
    public static function successCreateWithData(string $message,
                                                 $data = null) : Response
    {
        return response()->json([
            'status'  => Config::get('offers-go.status.success'),
            'message' => $message,
            'data'    => $data
        ], Response::HTTP_CREATED);
    }

    /**
     * Función que devuelve los datos en Response
     *
     * @param string
     * @return Response
     */
    public static function successProcessing(string $message) : Response
    {
        return response()->json([
            'status'  => Config::get('offers-go.status.success'),
            'message' => $message
        ], Response::HTTP_PROCESSING);
    }
}
