<?php namespace App\Http\Controllers\Api;

use App\OffersGo\Helpers\ServiceHelper  as ServiceHelper;
use App\OffersGo\Helpers\ResponseHelper as ResponseHelper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Exception;

class UsuarioController extends Controller
{
    /* Public Properties
    -------------------------------*/

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['store']]);
    }

    /**
     * @OA\Get(
     *   path="/api/customers",
     *   summary="Busqueda de clientes",
     *   tags={"Clientes"},
     *   security={{"passport": {}}},
     *
     *   @OA\Response(response=200, description="Successful operation"),
     *   @OA\Response(response=400, description="Non-existent Resource"),
     *   @OA\Parameter(
     *     name="filter",
     *     in="query",
     *     description="Datos de Busqueda",
     *   ),
     * )
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $data  = [];
        $input = [
            'data'   => $request->input(),
            'method' => 'Index'
        ];

        try
        {
            // Procesamos la visualizacion de usuarios
            ServiceHelper::make('Usuario')
                ->process($input)
                ->get($data);
        }
        catch (Exception $Error)
        {
            return ResponseHelper::errorRequest(
                trans('controller.index.error'),
                $Error->getCode(),
                $Error->getMessage()
            );
        }

        // Devolvemos los datos
        return ResponseHelper::successOkWithDataAndExtras(
            trans('controller.index.success'),
            $data['data'],
            $data['extras']
        );
    }

    public function create(){}

    /**
     * @OA\Post(
     *   path="/api/register",
     *   summary="Registra nuevo usuario",
     *   tags={"Usuario"},
     *
     *   @OA\Response(response=201, description="Succesfull operation"),
     *   @OA\Response(response=400, description="Non-existent Resource"),
     *   @OA\RequestBody(
     *     request="create",
     *     description="Datos de Creacion",
     *     required=true,
     *     @OA\JSonContent(
     *       @OA\Property(property="user", type="object",
     *           @OA\Property(property="name", type="string"),
     *           @OA\Property(property="email", type="string"),
     *           @OA\Property(property="password", type="string"),
     *           @OA\Property(property="password_confirmation", type="string")
     *       ),
     *        @OA\Property(property="address", type="object",
     *           @OA\Property(property="country", type="string"),
     *           @OA\Property(property="state", type="string"),
     *           @OA\Property(property="area_code", type="string"),
     *           @OA\Property(property="city", type="string")
     *           @OA\Property(property="region", type="string")
     *           @OA\Property(property="street", type="string")
     *           @OA\Property(property="floor", type="integer")
     *           @OA\Property(property="department_number", type="string")
     *           @OA\Property(property="latitude", type="string")
     *           @OA\Property(property="longitude", type="string")
     *       ),
     *        @OA\Property(property="customer", type="object",
     *           @OA\Property(property="firstname", type="string"),
     *           @OA\Property(property="lastname", type="string"),
     *           @OA\Property(property="phone_number", type="string"),
     *           @OA\Property(property="gender", type="char")
     *       )
     *     )
     *   )
     * )
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $data  = [];
        $input = [
            'data'   => $request->input(),
            'method' => 'Registrar'
        ];

        try
        {
            // Procesamos el registro de un usuario
            ServiceHelper::make('Usuario')
                ->process($input)
                ->get($data);
        }
        catch (Exception $Error)
        {
            return ResponseHelper::errorRequest(
                trans('controller.register.error'),
                $Error->getCode(),
                $Error->getMessage()
            );
        }

        // Devolvemos los datos
        return ResponseHelper::successCreateWithData(
            trans('controller.register.success'),
            $data
        );
    }

    /**
     * @OA\Get(
     *   path="/api/customers/{id}",
     *   summary="Busqueda de un cliente",
     *   tags={"Clientes"},
     *
     *   @OA\Response(response=200, description="Successful operation"),
     *   @OA\Response(response=400, description="Non-existent Resource"),
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     description="Id de Cliente",
     *   ),
     * )
     * @param $id
     * @return Response
     */
    public function show($id)
    {
        $data  = [];
        $input = [
            'data'   => $id,
            'method' => 'Show'
        ];

        try
        {
            // Procesamos la visualizacion de un usuario
            ServiceHelper::make('Usuario')
                ->process($input)
                ->get($data);
        }
        catch (Exception $Error)
        {
            return ResponseHelper::errorRequest(
                trans('controller.show.error'),
                $Error->getCode(),
                $Error->getMessage()
            );
        }

        // Devolvemos los datos
        return ResponseHelper::successOkWithData(
            trans('controller.show.success'),
            $data
        );
    }

    public function edit(){}

    /**
     * @OA\Put(
     *   path="/api/customers/{id}",
     *   summary="Modificacion de un cliente",
     *   tags={"Clientes"},
     *
     *   @OA\Response(response=201, description="Successful operation"),
     *   @OA\Response(response=400, description="Bad Request"),
     *   @OA\RequestBody(
     *     request="update",
     *     description="Datos de Actualizacion",
     *     required=true,
     *     @OA\JSonContent(
     *       @OA\Property(property="user", type="object",
     *           @OA\Property(property="name", type="string"),
     *           @OA\Property(property="email", type="string")
     *           @OA\Property(property="active", type="boolean")
     *       ),
     *        @OA\Property(property="address", type="object",
     *           @OA\Property(property="country", type="string"),
     *           @OA\Property(property="state", type="string"),
     *           @OA\Property(property="area_code", type="string"),
     *           @OA\Property(property="city", type="string")
     *           @OA\Property(property="region", type="string")
     *           @OA\Property(property="street", type="string")
     *           @OA\Property(property="floor", type="integer")
     *           @OA\Property(property="department_number", type="string")
     *           @OA\Property(property="latitude", type="string")
     *           @OA\Property(property="longitude", type="string")
     *       ),
     *        @OA\Property(property="customer", type="object",
     *           @OA\Property(property="firstname", type="string"),
     *           @OA\Property(property="lastname", type="string"),
     *           @OA\Property(property="phone_number", type="string"),
     *           @OA\Property(property="gender", type="char")
     *       )
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     description="Id de Cliente",
     *   ),
     * )
     * @param $id
     * @param Request $request
     * @return Response
     */
    public function update($id,
                           Request $request)
    {
        /*$input                   = $request->input();
        $input["customer"]["id"] = $id;

        $data = [];

        try
        {
            // Procesamos la actualización los datos
            ServiceHelper::make('Customer')
                ->updateProcess($input)
                ->get($data);
        }
        catch (Exception $Error)
        {
            return ResponseHelper::errorBadRequest(
                trans('feriame.update.error'),
                ($Error->getMessage() == 'E1' || $Error->getMessage() == 'E2')
                    ? null
                    : $Error->getMessage()
            );
        }

        // Devolvemos los datos
        return ResponseHelper::successHTTPWithData(
            trans('feriame.update.success'),
            $data
        );*/
    }

    /**
     * @OA\Delete(
     *   path="/api/customers/{id}",
     *   summary="Eliminacion de un clientes",
     *   tags={"Clientes"},
     *
     *   @OA\Response(response=200, description="Successful operation"),
     *   @OA\Response(response=400, description="Bad Request"),
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     description="Id de Cliente",
     *   ),
     * )
     * @param $id
     * @return Response
     */
    public function destroy($id)
    {
        $input = [
            'data'   => $id,
            'method' => 'Eliminar'
        ];

        try
        {
            // Procesamos la eliminacion de un usuario
            ServiceHelper::make('Usuario')
                ->process($input);
        }
        catch (Exception $Error)
        {
            return ResponseHelper::errorRequest(
                trans('controller.index.error'),
                $Error->getCode(),
                $Error->getMessage()
            );
        }

        // Devolvemos los datos
        return ResponseHelper::successProcessing(
            trans('controller.index.success')
        );
    }
}
