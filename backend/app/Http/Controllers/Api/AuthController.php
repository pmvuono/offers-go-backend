<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class AuthController extends Controller
{
    /**
     * Registro de usuario
     */
    public function signUp(Request $request)
    {
        $request->validate([
            User::NAME => 'required|string',
            User::EMAIL => 'required|email|max:100',
            User::PASSWORD => 'required|regex:/^.*(?=.*[a-zA-Z])(?=.*[0-9])/|min:8|max:16'
        ]);

        User::create([
            User::NAME => $request->nombre,
            User::EMAIL => $request->correo,
            User::PASSWORD => bcrypt($request->contrasenia)
        ]);

        return response()->json([
            'message' => 'Usuario creado!'
        ], Response::HTTP_CREATED);
    }

    /**
     * Inicio de sesión y creación de token
     */
    public function login(Request $request)
    {
        $request->validate([
            User::EMAIL => 'required|email|max:100',
            User::PASSWORD => 'required|regex:/^.*(?=.*[a-zA-Z])(?=.*[0-9])/|min:8|max:16',
            User::TOKEN => 'boolean'
        ]);

        $credentials = request([User::EMAIL, User::PASSWORD]);

        if (!Auth::attempt($credentials)) 
        {
            return response()->json([
                        'message' => 'Lograste Salir!!'
                            ], 200);
        }

        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');

        $token = $tokenResult->token;
        
        if ($request->recordar_token) 
        {
            $token->expires_at = Carbon::now()->addWeeks(1);
        }
        $token->save();

        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse($token->expires_at)->toDateTimeString()
        ]);
    }

    /**
     * Cierre de sesión (anular el token)
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();

        return response()->json([
            'message' => 'lograste Salir!'
        ]);
    }

    /**
     * Obtener el objeto User como json
     */
    public function user(Request $request)
    {
        return response()->json($request->user());
    }
}