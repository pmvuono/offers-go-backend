<?php namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasFactory,
        Notifiable,
        HasApiTokens;

    /* Public Attributes
    -------------------------------*/

    /**
     * Nombre de Campos
     */
    const EMAIL             = 'correo';
    const NAME              = 'nombre';
    const PASSWORD          = 'contrasenia';
    const IS_ADMIN          = 'administrador';
    const TOKEN             = 'recordar_token';
    const ACTIVE            = 'activo';
    const EMAIL_VERIFIED_AT = 'email_verified_at';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::NAME,
        self::EMAIL,
        self::PASSWORD,
        self::IS_ADMIN,
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        self::CREATED_AT,
        self::UPDATED_AT,
        self::PASSWORD,
        self::TOKEN,
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        self::EMAIL_VERIFIED_AT => 'datetime',
    ];
}
