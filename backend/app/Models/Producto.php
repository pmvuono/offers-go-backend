<?php namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    use HasFactory;

    /* Public Attributes
    -------------------------------*/

    /**
     * Nombre de Campos
     */
    const NAME      = 'nombre';
    const TYPE      = 'tipo';
    const STOCK     = 'stock';
    const PRICE     = 'precio';
    const ACTIVE    = 'active';
    const TIENDA_ID = 'tiendas_id';

    /* Protected Attributes
    -------------------------------*/

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::NAME,
        self::TYPE,
        self::STOCK,
        self::PRICE,
        self::ACTIVE,
        self::TIENDA_ID
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        self::CREATED_AT,
        self::UPDATED_AT,
        self::TIENDA_ID
    ];
    
   /* Protected Methods
    -------------------------------*/
     
    /**
     * Relación con Tienda
     *
     * @return BelongsTo
     */
    public function tienda()
    {
        return $this->belongsTo(
                Tienda::class,
                self::TIENDA_ID,
                'id'
        );
    }
}
