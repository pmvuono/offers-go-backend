<?php namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    use HasFactory;

    /* Public Attributes
    -------------------------------*/

    /**
     * Nombre de Campos
     */
    const FIRSTNAME = 'nombre';
    const LASTNAME  = 'apellido';
    const ACTIVE    = 'activo';
    const USER_ID   = 'users_id';

    /* Protected Attributes
    -------------------------------*/

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::FIRSTNAME,
        self::LASTNAME,
        self::ACTIVE,
        self::USER_ID,
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        self::CREATED_AT,
        self::UPDATED_AT,
        self::ACTIVE,
        self::USER_ID,
    ];
    
    /**
     * Relación con User
     *
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(
                User::class,
                self::USER_ID,
                'id'
        );
    }
}
