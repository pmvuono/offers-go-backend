<?php namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TiendaCalificacion extends Model
{
    use HasFactory;
    
   /* Public Attributes
    -------------------------------*/

    /**
     * Nombre de Campos
     */
    const RANKING   = 'calificacion';
    const COUNT     = 'cantidad';
    const ACTIVE    = 'activo';
    const TIENDA_ID = 'tiendas_id';

    /**
     * Propiedades del Modelo
     */
    protected $fillable = [
        self::RANKING,
        self::COUNT,
        self::ACTIVE,
        self::TIENDA_ID,
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        self::CREATED_AT,
        self::UPDATED_AT,
        self::ACTIVE,
        self::TIENDA_ID,
    ];
    
    /**
     * Relación con Tiendas
     *
     * @return BelongsTo
     */
    public function tienda()
    {
        return $this->belongsTo(
                Usuario::class,
                self::USUARIO_ID,
                'id'
        );
    }

}
