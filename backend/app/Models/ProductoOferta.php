<?php namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductoOferta extends Model
{
    use HasFactory;
    
    /* Public Attributes
    -------------------------------*/

    /**
     * Nombre de Campos
     */
    const DATE_FROM   = 'fecha_desde';
    const DATE_TO     = 'fecha_hasta';
    const TEXT        = 'texto';
    const ACTIVE      = 'activo';
    const PRODUCTO_ID = 'productos_id';
    const TIENDA_ID   = 'tiendas_id';

    /**
     * Propiedades del Modelo
     */
    protected $fillable = [
        self::DATE_FROM,
        self::DATE_TO,
        self::TEXT,
        self::ACTIVE,
        self::PRODUCTO_ID,
        self::TIENDA_ID
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        self::CREATED_AT,
        self::UPDATED_AT,
        self::ACTIVE,
        self::PRODUCTO_ID,
        self::TIENDA_ID
    ];
    
    /* Protected Methods
    -------------------------------*/
    
    /**
     * Relación con Productos
     *
     * @return BelongsTo
     */
    public function producto()
    {
        return $this->belongsTo(
                Producto::class,
                self::PRODUCTO_ID,
                'id'
        );
    }
    
    /**
     * Relación con Tiendas
     *
     * @return BelongsTo
     */
    public function tienda()
    {
        return $this->belongsTo(
                Producto::class,
                self::TIENDA_ID,
                'id'
        );
    }
}
