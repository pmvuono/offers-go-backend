<?php namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tienda extends Model
{
    use HasFactory;

/* Public Attributes
    -------------------------------*/

    /**
     * Nombre de Campos
     */
    const CUIT       = 'cuit';
    const STATE      = 'provincia';
    const CITY       = 'partido';
    const REGION     = 'localidad';
    const STREET     = 'calle';
    const FLOOR      = 'piso';
    const DEP_NUMBER = 'numero_departamento';
    const ACTIVE     = 'activo';
    const USER_ID    = 'users_id';

    /* Protected Attributes
    -------------------------------*/

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::CUIT,
        self::STATE,
        self::CITY,
        self::REGION,
        self::STREET,
        self::FLOOR,
        self::DEP_NUMBER,
        self::ACTIVE,
        self::USER_ID,
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        self::CREATED_AT,
        self::UPDATED_AT,
        self::ACTIVE,
        self::USER_ID,
    ];
    
    /**
     * Relación con User
     *
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(
                User::class,
                self::USER_ID,
                'id'
        );
    }
}
