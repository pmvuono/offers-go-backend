<?php namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UsuarioCalificacion extends Model
{
    use HasFactory;

    /* Public Attributes
    -------------------------------*/

    /**
     * Nombre de Campos
     */
    const RANKING    = 'calificacion';
    const COUNT      = 'cantidad';
    const ACTIVE     = 'activo';
    const USUARIO_ID = 'usuarios_id';

    /**
     * Propiedades del Modelo
     */
    protected $fillable = [
        self::RANKING,
        self::COUNT,
        self::ACTIVE,
        self::USUARIO_ID,
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        self::CREATED_AT,
        self::UPDATED_AT,
        self::ACTIVE,
        self::USUARIO_ID,
    ];

    /* Protected Methods
    -------------------------------*/
    
    /**
     * Relación con Usuarios
     *
     * @return BelongsTo
     */
    public function usuario()
    {
        return $this->belongsTo(
                Usuario::class,
                self::USUARIO_ID,
                'id'
        );
    }
}
