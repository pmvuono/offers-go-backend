<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\ProductoOferta;

class CreateProductoOfertasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('producto_ofertas', function (Blueprint $table) {
            $table->id();
            
            $table->dateTime(ProductoOferta::DATE_FROM);
            $table->dateTime(ProductoOferta::DATE_TO);
            $table->string(ProductoOferta::TEXT, 255);
            $table->boolean(ProductoOferta::ACTIVE)->default(true);

            $table->unsignedBigInteger(ProductoOferta::PRODUCTO_ID)->unsigned();
            $table->foreign(ProductoOferta::PRODUCTO_ID)->references('id')->on('productos');
            
            $table->unsignedBigInteger(ProductoOferta::TIENDA_ID)->unsigned();
            $table->foreign(ProductoOferta::TIENDA_ID)->references('id')->on('tiendas');
           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('producto_ofertas');
    }
}
