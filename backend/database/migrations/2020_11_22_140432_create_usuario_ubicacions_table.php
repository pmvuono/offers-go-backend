<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\UsuarioUbicacion;

class CreateUsuarioUbicacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuario_ubicacions', function (Blueprint $table) {
            $table->id();
            
            $table->BigInteger(UsuarioUbicacion::LATITUDE);
            $table->BigInteger(UsuarioUbicacion::LONGITUDE);
            $table->boolean(UsuarioUbicacion::ACTIVE)->default(true);

            $table->unsignedBigInteger(UsuarioUbicacion::USUARIO_ID)->unsigned();
            $table->foreign(UsuarioUbicacion::USUARIO_ID)->references('id')->on('usuarios');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuario_ubicacions');
    }
}
