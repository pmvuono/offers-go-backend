<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Producto;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->id();
            
            $table->string(Producto::NAME, 50);
            $table->string(Producto::TYPE, 50);
            $table->unsignedInteger(Producto::STOCK);
            $table->float(Producto::PRICE);
            $table->boolean(Producto::ACTIVE)->default(true);

            $table->unsignedBigInteger(Producto::TIENDA_ID)->unsigned();
            $table->foreign(Producto::TIENDA_ID)->references('id')->on('tiendas');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
