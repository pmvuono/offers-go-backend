<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\UsuarioCalificacion;

class CreateUsuarioCalificacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuario_calificacions', function (Blueprint $table) {
            $table->id();            
            
            $table->float(UsuarioCalificacion::RANKING);
            $table->string(UsuarioCalificacion::COUNT, 10);
            $table->boolean(UsuarioCalificacion::ACTIVE)->default(true);

            $table->unsignedBigInteger(UsuarioCalificacion::USUARIO_ID)->unsigned();
            $table->foreign(UsuarioCalificacion::USUARIO_ID)->references('id')->on('usuarios');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuario_calificacions');
    }
}
