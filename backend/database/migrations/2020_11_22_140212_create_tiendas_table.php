<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Tienda;

class CreateTiendasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tiendas', function (Blueprint $table) {
            $table->id();
            
            $table->string(Tienda::CUIT, 20);
            $table->string(Tienda::STATE, 50);
            $table->string(Tienda::CITY, 50);
            $table->string(Tienda::REGION, 50);
            $table->string(Tienda::STREET, 50);
            $table->unsignedInteger(Tienda::FLOOR);
            $table->string(Tienda::DEP_NUMBER, 10);
            $table->boolean(Tienda::ACTIVE)->default(true);

            $table->unsignedBigInteger(Tienda::USER_ID)->unsigned();
            $table->foreign(Tienda::USER_ID)->references('id')->on('users');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tiendas');
    }
}
