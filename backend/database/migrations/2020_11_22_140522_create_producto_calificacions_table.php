<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\ProductoCalificacion;

class CreateProductoCalificacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('producto_calificacions', function (Blueprint $table) {
            $table->id();            
            
            $table->float(ProductoCalificacion::RANKING);
            $table->string(ProductoCalificacion::COUNT, 10);
            $table->boolean(ProductoCalificacion::ACTIVE)->default(true);

            $table->unsignedBigInteger(ProductoCalificacion::PRODUCTO_ID)->unsigned();
            $table->foreign(ProductoCalificacion::PRODUCTO_ID)->references('id')->on('productos');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('producto_calificacions');
    }
}
