<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\TiendaCalificacion;

class CreateTiendaCalificacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tienda_calificacions', function (Blueprint $table) {
            $table->id();            
            
            $table->float(TiendaCalificacion::RANKING);
            $table->string(TiendaCalificacion::COUNT, 10);
            $table->boolean(TiendaCalificacion::ACTIVE)->default(true);

            $table->unsignedBigInteger(TiendaCalificacion::TIENDA_ID)->unsigned();
            $table->foreign(TiendaCalificacion::TIENDA_ID)->references('id')->on('tiendas');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tienda_calificacions');
    }
}
