<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use \App\Models\Usuario;

class CreateUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->id();
            
            $table->string(Usuario::FIRSTNAME, 50);
            $table->string(Usuario::LASTNAME, 50);
            $table->boolean(Usuario::ACTIVE)->default(true);

            $table->unsignedBigInteger(Usuario::USER_ID)->unsigned();
            $table->foreign(Usuario::USER_ID)->references('id')->on('users');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}
