<?php namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Usuario;

class UsuarioTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        $categories = [
            [
                Usuario::FIRSTNAME => 'Jessy',
                Usuario::LASTNAME => 'Pinkman',
                Usuario::USER_ID => 1
            ],
            [
                Usuario::FIRSTNAME => 'Walter',
                Usuario::LASTNAME => 'White',
                Usuario::USER_ID => 2
            ],
            [
                Usuario::FIRSTNAME => 'Breaking',
                Usuario::LASTNAME => 'Bad',
                Usuario::USER_ID => 3
            ]
        ];

        foreach ($categories as $category)
        {
            Usuario::create($category);
        }
    }
}
