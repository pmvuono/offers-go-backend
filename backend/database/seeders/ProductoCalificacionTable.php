<?php namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ProductoCalificacion;

class ProductoCalificacionTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                ProductoCalificacion::RANKING => '4.6',
                ProductoCalificacion::COUNT => 30,
                ProductoCalificacion::PRODUCTO_ID => 1
            ],
            [
                ProductoCalificacion::RANKING => '4.8',
                ProductoCalificacion::COUNT => 10,
                ProductoCalificacion::PRODUCTO_ID => 2
            ]
        ];

        foreach ($categories as $category)
        {
            ProductoCalificacion::create($category);
        }
    }
}
