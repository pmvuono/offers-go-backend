<?php namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\UsuarioCalificacion;

class UsuarioCalificacionTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                UsuarioCalificacion::RANKING => '4.6',
                UsuarioCalificacion::COUNT => 30,
                UsuarioCalificacion::USUARIO_ID => 1
            ],
            [
                UsuarioCalificacion::RANKING => '3.2',
                UsuarioCalificacion::COUNT => 40,
                UsuarioCalificacion::USUARIO_ID => 2
            ]
            
        ];

        foreach ($categories as $category)
        {
            UsuarioCalificacion::create($category);
        }
    }
}
