<?php namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Producto;

class ProductoTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        $categories = [
            [
                Producto::NAME => 'Pantalon',
                Producto::TYPE => 'Indumentaria',
                Producto::STOCK => 200,
                Producto::PRICE => '1270',
                Producto::TIENDA_ID => 1
            ],
            [
                Producto::NAME => 'Zapatilla',
                Producto::TYPE => 'Calzado',
                Producto::STOCK => 10,
                Producto::PRICE => '12720',
                Producto::TIENDA_ID => 1
            ]
        ];

        foreach ($categories as $category)
        {
            Producto::create($category);
        }
    }
}
