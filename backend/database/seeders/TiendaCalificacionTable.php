<?php namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\TiendaCalificacion;

class TiendaCalificacionTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                TiendaCalificacion::RANKING => '4.6',
                TiendaCalificacion::COUNT => 30,
                TiendaCalificacion::TIENDA_ID => 1
            ]
        ];

        foreach ($categories as $category)
        {
            TiendaCalificacion::create($category);
        }
    }
}
