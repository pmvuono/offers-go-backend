<?php namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use App\Models\ProductoOferta;

class ProductoOfertaTable extends Seeder
{    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                ProductoOferta::DATE_FROM => Carbon::create('2020', '10', '10'),
                ProductoOferta::DATE_TO => Carbon::create('2020', '10', '10'),
                ProductoOferta::TEXT => 'Oferta de pantalones',
                ProductoOferta::PRODUCTO_ID => 1,
                ProductoOferta::TIENDA_ID => 1
            ],
            [
                ProductoOferta::DATE_FROM => Carbon::create('2020', '10', '10'),
                ProductoOferta::DATE_TO => Carbon::create('2020', '10', '10'),
                ProductoOferta::TEXT => 'Oferta de zapatillas',
                ProductoOferta::PRODUCTO_ID => 2,
                ProductoOferta::TIENDA_ID => 1
            ]
        ];

        foreach ($categories as $category)
        {
            ProductoOferta::create($category);
        }
    }
}
