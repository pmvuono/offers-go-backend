<?php namespace Database\Seeders;

use Illuminate\Database\Seeder;
use \App\Models\UsuarioUbicacion;
use Faker\Factory as Faker;

class UsuarioUbicacionTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {       
        $faker = Faker::create('es_ES');
                
        $categories = [
            [
                UsuarioUbicacion::LATITUDE => $faker->latitude,
                UsuarioUbicacion::LONGITUDE => $faker->longitude,
                UsuarioUbicacion::USUARIO_ID => 1
            ],
            [
                UsuarioUbicacion::LATITUDE => $faker->latitude,
                UsuarioUbicacion::LONGITUDE => $faker->longitude,
                UsuarioUbicacion::USUARIO_ID => 1
            ]
        ];

        foreach ($categories as $category)
        {
            UsuarioUbicacion::create($category);
        }
    }
}
