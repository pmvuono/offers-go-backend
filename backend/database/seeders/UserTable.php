<?php namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UserTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
        $categories = [
            [
                User::NAME => 'jpinkman',
                User::EMAIL => 'jpinkman@bb.com',
                User::IS_ADMIN => true,
                User::PASSWORD => bcrypt('breaking1')
            ],
            [
                User::NAME => 'wwhite',
                User::EMAIL => 'wwhite@bb.com',
                User::PASSWORD => bcrypt('breaking2')
            ],
            [
                User::NAME => 'BreakingBad',
                User::EMAIL => 'breaking@bad.com',
                User::PASSWORD => bcrypt('breaking3')
            ]
        ];

        foreach ($categories as $category)
        {
            User::create($category);
        }
    }
}
