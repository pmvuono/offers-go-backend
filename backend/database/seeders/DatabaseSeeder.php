<?php  namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Eloquent;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

        // Modulo User
        $this->call(UserTable::class);

        // Modulo Usuario
        $this->call(UsuarioTable::class);
        $this->call(UsuarioCalificacionTable::class);
        $this->call(UsuarioUbicacionTable::class);

        // Modulo Tienda
        $this->call(TiendaTable::class);
        $this->call(TiendaCalificacionTable::class);

        // Modulo producto
        $this->call(ProductoTable::class);
        $this->call(ProductoCalificacionTable::class);
        $this->call(ProductoOfertaTable::class);
        
        Eloquent::reguard();
    }
}
