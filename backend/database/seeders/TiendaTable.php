<?php namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Tienda;
use Faker\Factory as Faker;

class TiendaTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        $faker = Faker::create('es_ES');
        
        $categories = [
            [
                Tienda::CUIT => '10765234',
                Tienda::STATE => $faker->state,
                Tienda::CITY => $faker->city,
                Tienda::REGION => 'Region_01',
                Tienda::STREET => $faker->streetName,
                Tienda::FLOOR => '10',
                Tienda::DEP_NUMBER => '10B',
                Tienda::USER_ID => 3
            ]
        ];

        foreach ($categories as $category)
        {
            Tienda::create($category);
        }
    }
}
