<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

/*Route::group(['middleware' => ['auth:api']],
    function ()
    {
        Route::resource('logout', 'App\Http\Controllers\Auth\LogoutController',
            ['only'=>['store']]
        );
    });*/

Route::group(['auth:api'],
    function ()
    {
        /*Route::resource('login', 'App\Http\Controllers\Auth\LoginController',
            ['only'=>['store']]
        );*/

        Route::resource('usuarios', 'App\Http\Controllers\Api\UsuarioController',
            ['only'=>['index', 'store', 'show', 'update', 'destroy']]
        );

    });

    
Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'App\Http\Controllers\Api\AuthController@login');
    Route::post('signup', 'App\Http\Controllers\Api\AuthController@signUp');

    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'App\Http\Controllers\Api\AuthController@logout');
        Route::get('user', 'App\Http\Controllers\Api\AuthController@user');
    });
});
